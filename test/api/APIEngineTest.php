<?php
require '../../vendor/autoload.php';
use PHPUnit\Framework\TestCase;

class APIEngineTest extends TestCase {
  public $http;

  public function setUp() {
    $this->http = new \GuzzleHttp\Client([
      'base_uri' => 'http://localhost/PandaHouse/api',
      'exceptions' => false
    ]);
  }

  public function testApiCallOk() {

  }

  public function testWrongRouteNameShouldError() {
    $res = $this->http->post('?sec.auth', [
      'form_params' => [
        'key' => '',
        'param' => '{}'
      ]
    ]);

    $this->assertEquals(400, $res->getStatusCode());
  }
}
