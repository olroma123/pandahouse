<?php
require '../../api/lib/APIValidator.php';

class ValidatorTest extends PHPUnit\Framework\TestCase {
  public function testValidateParamsOk() {
    $result = APIValidator::validateParams(['login' => 'olroma'], ['login']);
    $this->assertTrue($result);
  }

  public function testValidateParamsNotAllRequiredFieldsReturnFalse() {
    $result = APIValidator::validateParams(['login' => 'olroma'], ['login', 'password']);
    $this->assertFalse($result);
  }

  public function testValidateParamsAdditionalParamsReturnFalse() {
    $result = APIValidator::validateParams(['login' => 'olroma', 'testApiGateway' => 'testApiGateway'], ['login']);
    $this->assertFalse($result);
  }

  public function testValidateParamsRequiredParamIsEmptyReturnFalse() {
    $result = APIValidator::validateParams(['login' => ''], ['login']);
    $this->assertFalse($result);
  }
}