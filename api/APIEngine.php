<?php

class APIEngine {
  private $client;

  public function __construct($info) {
    $this->client = $info;
  }

  protected function securityMethods() {
    require './routes/Security.php';

    switch (strtolower($this->client['routeInfo']['method'])) {
      case 'auth': {
        SecurityAPI::authorize($this->client);
        break;
      }

      case 'register': {
        SecurityAPI::register($this->client);
        break;
      }

      default: {
        http_response_code(400);
        exit(\APIMessage\Error\Security\Security::wrongMethodOrRoute());
      }
    }
  }

  protected function routePipe() {
    require './lib/Database.php';
    require './models/UserModel.php';

    switch (strtolower($this->client['routeInfo']['route'])) {
      case 'security': {
        $this->securityMethods();
        break;
      }

      default: {
        http_response_code(400);
        exit(\APIMessage\Error\Security\Security::wrongMethodOrRoute());
      }
    }
  }

  public function run() {
    $this->routePipe();
  }
}