<?php
class SecurityAPI {
  public static function authorize($client) {

  }

  /**
   * Validate the Email syntax
   * @param string $email The email of user
   * @return bool Returns True if email format is valid
  */
  protected static function _validateEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  /**
   * Validate the Full Name syntax
   * @param string $fullName The full name of user
   * @return bool Returns True if full name's format is ok
   */
  protected static function _validateFullName($fullName) {
    return preg_match('/^[А-ЯЁ][а-яё]+(-[А-ЯЁ][а-яё]+)? [А-ЯЁ][а-яё]+( [А-ЯЁ][а-яё]+)?$/u', $fullName);
  }

  /**
   * Method for register the account
  */
  public static function register($client) {
    // Validate required params
    if (!APIValidator::validateParams($client['paramInfo'], ['email', 'password', 'fullName'])) {
      http_response_code(400);
      exit(\APIMessage\Error\Security\Security::notAllParamsWhereIncluded());
    }

    //normalize the full name
    $client['paramInfo']['fullName'] = ucwords($client['paramInfo']['fullName'], ' ');

    // validate the syntax of params
    if (!self::_validateEmail($client['paramInfo']['email'])) {
      http_response_code(406);
      exit(\APIMessage\Error\Security\Security::wrongEmailFormat());
    } else if (!self::_validateFullName($client['paramInfo']['fullName'])) {
      http_response_code(406);
      exit(\APIMessage\Error\Security\Security::wrongFullNameFormat());
    }

    if (UserModel::isAccountExistsByEmail($client['paramInfo']['email'])) {
      http_response_code(409);
      exit(\APIMessage\Error\Security\Security::accountExists());
    }

    // TODO: make token validation by existing or not
    $token = str_replace('-', '', Uuid::genUuid());
    $password = password_hash($client['paramInfo']['password'], PASSWORD_DEFAULT);

    $db = new Database();
    try {
      $query = $db->db->prepare('INSERT INTO users (Password, Email, Token, FullName, RoleID) VALUES (?, ?, ?, ?, ?)');
      $query->execute([$password, $client['paramInfo']['email'], $token, $client['paramInfo']['fullName'], 2]);
      $db->close();
    } catch (PDOException $ex) {
      exit(\APIMessage\Error\Security\Security::serverError($ex));
    }

    exit(\APIMessage\Success\Security\Security::registerSuccess());
  }
}