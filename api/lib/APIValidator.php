<?php
/**
 * Created by PhpStorm.
 * User: OlegRom4ig
 * Date: 8/18/2018
 * Time: 10:20 AM
 */

class APIValidator {
  /**
   * Method for validate the params in body by checking their name
   * Also check for additional params. If it exists return false
   * @param array $actualParams An object with transferred params
   * @param array $expectedParams An array with string's name of params
   * @return bool Returns the True if params be validated success
  */
  public static function validateParams($actualParams, $expectedParams) {
    if (!isset($actualParams) || count($actualParams) !== count($expectedParams)) return false;

    $actualKeys = array_keys($actualParams);
    for ($i = 0; $i < count($expectedParams); $i++) {
      // If required param is not in actual array or we have additional key
      // Then we drop the error to client
      if (!in_array($actualKeys[$i], $expectedParams) || $actualParams[$actualKeys[$i]] === '') return false;
    }

    return true;
  }
}