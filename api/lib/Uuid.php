<?php
class Uuid {
  /**
   * Generating the 36 char's uuid
   * @return string Returns the uuid 36 char
   * @throws Throwable fine
  */
  public static function genUuid() {
    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      random_int(0, 0xffff), random_int(0, 0xffff),
      random_int(0, 0xffff),
      random_int(0, 0x0fff) | 0x4000,
      random_int(0, 0x3fff) | 0x8000,
      random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
    );
  }
}