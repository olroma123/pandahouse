<?php
/**
 * Created by PhpStorm.
 * User: OlegRom4ig
 * Date: 8/19/2018
 * Time: 11:38 AM
 */

class Database {
  /**
   * @var PDO Db object
  */
  public $db;

  /**
   * Get the connection options
   * @return array Returns an object with info about connection with database
  */
  protected function getConnectionInfo() {
    $login = "OlegRom4ig";
    $psswd = "25olroma1998";
    $dbName = "dbname=pandahousedev;";
    $dbHost = "mysql:host=localhost;";
    $charSet = "charset=utf8";
    $connStr = $dbHost . $dbName . $charSet;

    return [
      'connectionString' => $connStr,
      'credits' => [
        'login' => $login,
        'password' => $psswd
      ]
    ];
  }

  /**
   * Connect to database
   * @return bool Returns TRUE if connection established
  */
  public function connect() {
    $dbInfo = self::getConnectionInfo();

    try {
      $this->db = new PDO($dbInfo['connectionString'], $dbInfo['credits']['login'], $dbInfo['credits']['password']);
      $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $this->db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
      return false;
    }

    return true;
  }

  public function close() {
    $this->db = null;
  }

  public function __construct($connectInstant = true) {
    if ($connectInstant && !$this->connect()) {
      http_response_code(500);
      exit(\APIMessage\Error\Database\Database::cannotConnect());
    }
  }
}