<?php
namespace APIMessage\Error\Database;
use APIMessage\Creator;

class Database extends Creator {
  public static function cannotConnect() {
    return self::createErrorMessage('Ошибка сервера: невозможно подключиться к базе данных');
  }
}