<?php
/**
 * Created by PhpStorm.
 * User: OlegRom4ig
 * Date: 8/17/2018
 * Time: 8:53 PM
 */

namespace APIMessage\Error\Security;
use APIMessage\Creator;

class Security extends Creator {
  public static function accessDenied() {
      return self::createErrorMessage('Доступ в систему запрещен!');
  }

  public static function wrongSyntaxOfRequest() {
      return self::createErrorMessage('Параметры не были переданы правильно!');
  }

  public static function serverError($error) {
    $uuid = \Uuid::genUuid();
    error_log("ERROR - [{$uuid}]: {$error}");
    return self::createErrorMessage("Произошла ошибка на стороне сервера. Свяжитесь с администратором и сообщите ему этот ключ для решения данной проблемы: {$uuid}");
  }

  public static function notAllParamsWhereIncluded() {
    return self::createErrorMessage('Не все параметры были переданы!');
  }

  public static function wrongMethodOrRoute() {
    return self::createErrorMessage('Вызываемый метод не был найден!');
  }

  public static function wrongUserCredits() {
      return self::createErrorMessage('Неправильный логин или пароль!');
  }

  public static function wrongEmailFormat() {
    return self::createErrorMessage('Неправильный формат E-mail. Пример: house@example.ru');
  }

  public static function wrongFullNameFormat() {
    return self::createErrorMessage('Неправильный формат ФИО. Пример: Морозов Олег Романович');
  }

  public static function accountExists() {
    return self::createErrorMessage('Аккаунт с таким e-mail уже зарегистрирован');
  }
}