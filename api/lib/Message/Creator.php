<?php
/**
 * Created by PhpStorm.
 * User: OlegRom4ig
 * Date: 8/17/2018
 * Time: 8:46 PM
 */

namespace APIMessage;
class Creator {
    public static function createMessage($type, $message, $data = []) {
        return json_encode(['message' => ['type' => $type, 'text' => $message], 'data' => $data]);
    }

    public static function createErrorMessage($message, $data = []) {
        return self::createMessage('error', $message, $data);
    }

    public static function createSuccessMessage($message, $data = []) {
        return self::createMessage('success', $message, $data);
    }
}