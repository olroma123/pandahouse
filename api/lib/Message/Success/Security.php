<?php
/**
 * Created by PhpStorm.
 * User: OlegRom4ig
 * Date: 8/17/2018
 * Time: 8:53 PM
 */

namespace APIMessage\Success\Security;
use APIMessage\Creator;

class Security extends Creator {
  public static function registerSuccess() {
    return self::createSuccessMessage("Вы успешно зарегистрировались!");
  }
}