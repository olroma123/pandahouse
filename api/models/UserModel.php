<?php
class UserModel {
  private static $usersAuthInfo = 'users.Email, users.Token, users.FullName, roles.TranslatedTitle';

  private static function getUserBy($data, $query) {
    $db = new Database();
    $query = $db->db->prepare($query);
    $query->execute($data);
    $result = $query->fetch();
    $db->close();
    return $result;
  }

  public static function getFullNameObject($fullName) {
    $splitFullName = explode(' ', $fullName);

    $fullNameObj = [
      'first' => $splitFullName[1],
      'second' => $splitFullName[0],
    ];

    if (count($splitFullName) === 3) $fullNameObj['last'] = $splitFullName[3];
    return $fullNameObj;
  }

  private static function filterUserForAuth($dbUserData) {
    return [
      'email' => $dbUserData['Login'],
      'fullName' => self::getFullNameObject($dbUserData['FullName']),
      'token' => $dbUserData['Token']
    ];
  }

  public static function getUserByToken($token) {
    $result = self::getUserBy([$token],"SELECT * FROM users INNER JOIN roles ON users.RoleID = roles.ID WHERE users.Token = ?");
    echo self::filterUserForAuth($result);
  }

  public static function getUserByID($id) {
    $result = self::getUserBy([$id],"SELECT * FROM users INNER JOIN roles ON users.RoleID = roles.ID WHERE users.ID = ?");
    echo self::filterUserForAuth($result);
  }

  public static function getUserByEmailAndPassword($email, $password) {

  }

  /**
   * Is account is exists in db by email
   * @param string $email The user's email
   * @return bool Return TRUE if email is exists
  */
  public static function isAccountExistsByEmail($email) {
    $result = self::getUserBy([$email],"SELECT Count(*) as `Count` FROM users WHERE users.Email = ?");
    return $result['Count'] > 0;
  }
}