<?php
/**
 * API Gateway: /api/v1
 * Method: POST Only
 * Default Request: POST /api/v1?route=method BODY: key=&param={}
*/
header('Content-Type: application/json');
require 'lib/lib.php';
require './APIEngine.php';

class APIGateway {
  private $request;

  public function __construct() {
    $this->request = $_SERVER;
  }

  /**
   * Method for validate the request uri
   * @return bool Returns True if request is valid
  */
  public function validateRequestUri() {
    // Just simple validate
    if (!isset($this->request) || $this->request['QUERY_STRING'] === '' || $this->request['REQUEST_METHOD'] !== 'POST' || !isset($_POST['param']))
      return false;

    // validate query syntax
    // Should be a: ?route.method
    // And only 2 params: route AND method
    $methodInfo = explode('.', $this->request['QUERY_STRING']);
    if (count($methodInfo) > 2 || count($methodInfo) < 2) return false;

    return true;
  }

  /**
   * Method for make an object with route and method info
   * @return array Returns an object with [route => routeName, method => methodName]
  */
  public function getRouteInfo() {
    $methodInfo = explode('.', $this->request['QUERY_STRING']);

    return [
      'route' => $methodInfo[0],
      'method' => $methodInfo[1]
    ];
  }

  /**
   * Parse the JSON from "param" of body
   * @return bool | string Return the json object or False if wrong JSON format
  */
  public function parseBodyParams() {
    $tmp = json_decode($_POST['param'], true);
    if (is_null($tmp)) return false;
    return $tmp;
  }

  public function run() {
    if (!$this->validateRequestUri()) {
      http_response_code(400);
      exit(APIMessage\Error\Security\Security::wrongSyntaxOfRequest());
    }

    $apiEngine = new APIEngine([
      'routeInfo' => $this->getRouteInfo(),
      'paramInfo' => $this->parseBodyParams(),
      'token' => $_POST['key']
    ]);

    $apiEngine->run();
  }
}

$api = new APIGateway();
$api->run();
